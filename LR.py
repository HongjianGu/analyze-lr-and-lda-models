#!/usr/bin/env python
# coding: utf-8
import numpy as np
import math


class logisticRegression:

    def __init__(self, learningRate=0.1, iterN=1000):
        self.learningRate = learningRate
        self.iterN = iterN
        self.w = []

    def normalize(self, x):
        if max(x) == min(x):
            return x
        return (x) / (max(x) - min(x))
    
    def sigmoid(self, a):
        return (1 / (1 + np.exp(-a)))

    def fit1(self, X, y):
        num_samples, num_features = X.shape
        X = np.c_[X, np.ones(num_samples)]  # add a column of 1 at the end of features
        
        self.w = np.ones(num_features+1)/2

        for i in tqdm(range(self.iterN)):
            w = self.w
            for j in range(X.shape[0]):
                err = y[j] - self.sigmoid(np.dot(np.transpose(w), np.transpose(X[j])))

                self.w = self.w + self.learningRate * err * X[j]

    def fit(self, X, y):
        self.learningRate = 0.01
        self.fit1(X, y)
        self.learningRate = 0.001
        self.fit1(X, y)
        self.learningRate = 0.0001
        self.fit1(X, y)
        
    def predict(self, X):
        X = np.c_[X, np.ones(X.shape[0])]
        size = X.shape[0]
        target = np.full((size, 1), 0)

        for i in range(size):
            wTx = np.dot(X[i], self.w)
            z = self.sigmoid(wTx)
            if z >= 0.5:
                target[i] = 1
            else:
                target[i] = 0
        return target


# In[ ]:




