import numpy as np
import math


class logisticRegression:

    def __init__(self, learningRate=0.01, iterN=100):
        self.learningRate = learningRate
        self.iterN = iterN
#         num_samples, num_features = self.X.shape
        # self.w = [0.1 for i in range(len(self.X[0]) + 1)]
#         self.w = np.full((num_features, 1), 1)
        self.w = []
#         self.X = X
#         self.y = y

    def sigmoid(self, a):
        return 1 / (1 + math.exp(-a))

    def fit(self, X, y):
        num_samples, num_features = X.shape
        X = np.c_[X, np.ones(num_samples)]  # add a column of 1 at the end of features
        # self.y = np.reshape(self.y, (self.num_samples, 1))
        self.w = np.ones(num_features+1)

        for i in range(self.iterN):
            for j in range(X.shape[1]):
                err = y[j] - self.sigmoid(np.dot(X[j], np.transpose(self.w)))
                self.w[j] = self.w[j] + self.learningRate * np.sum(np.dot(X[j], err))

    def predict(self, X):
        X = np.c_[X, np.ones(X.shape[0])]
        size = X.shape[0]
        target = np.full((size, 1), 0)
        # wTx = self.X.dot(self.w)
        # z = self.sigmoid(wTx)
        for i in range(size):
            wTx = np.dot(X[i], self.w)
            z = self.sigmoid(wTx)
            if z >= 0.5:
                target[i] = 1
            else:
                target[i] = 0
        return target