# import numpy as np
# import pandas as pd
# import csv
# from tqdm import tqdm
# import math
# import seaborn as sns
# import matplotlib.pyplot as plt

import numpy as np
import csv
import math
import matplotlib.pyplot as plt

# from task1 import t1_1, t1_2, t1_3


# Blair 9/21: more matplotlib and fit model (words)
import numpy as np
import csv
import matplotlib.pyplot as plt

# t1_1 load datasets

dataWine = open('winequality-red.csv', 'r')

dataCancer = open('breast-cancer-wisconsin.data', 'r')

''' Some quotes on dataset
Wine:
fixed acidity;"volatile acidity";"citric acid";"residual sugar";"chlorides";
    "free sulfur dioxide";"total sulfur dioxide";"density";"pH";"sulphates";"alcohol";"quality"
    
Cancer:
7. Attribute Information: (class attribute has been moved to last column)
   #  Attribute                     Domain
   -- -----------------------------------------
   1. Sample code number            id number
   2. Clump Thickness               1 - 10
   3. Uniformity of Cell Size       1 - 10
   4. Uniformity of Cell Shape      1 - 10
   5. Marginal Adhesion             1 - 10
   6. Single Epithelial Cell Size   1 - 10
   7. Bare Nuclei                   1 - 10
   8. Bland Chromatin               1 - 10
   9. Normal Nucleoli               1 - 10
  10. Mitoses                       1 - 10
  11. Class:                        (2 for benign, 4 for malignant)
    
9. Class distribution:
   Benign: 458 (65.5%)
   Malignant: 241 (34.5%)
'''


# def normalize(x):
#     if max(x) == min(x):
#         return x
#     return x / (max(x) - min(x))

def isnumeric(str):
    try:
        float(str)
        return True
    except ValueError:
        return False

def t1_1(fileOpened, deliminater, headerExist):
    lines = [line.split() for line in fileOpened]
    if (headerExist == 1):  # dataWine
        contentsString = lines[1:]
    if (headerExist == 0):  # dataCancer
        contentsString = lines

        # this is to store the well-structured data, wine quality not classified yet
    contentsData = []
    i = 0
    print(len(contentsString))
    while i < len(contentsString):
        data = []
        dataSep = contentsString[i][0].split(deliminater)  # ";"
        for token in dataSep:
            # print (type (token))
            # data.append(float(token))     # switch to float at clean stage
            data.append(token)
        data = np.array(data)
        contentsData.append(data)
        i += 1

    contentsData = np.array(contentsData)
    return contentsData  # <class 'numpy.ndarray'>


wine_data = t1_1(dataWine, ";", 1)
# print(wine_data.shape)
# print(wine_data)

cancer_data = t1_1(dataCancer, ",", 0)


# print(cancer_data.shape)
# print(cancer_data)


# binary classification
# return a binaryY array (matrix), where the corresponding element is 1 when quality is {6,7,8,9,10},
# and is 0 when otherwise
def t1_2(contentsData):
    binaryY = np.array([])

    for entry in contentsData:

        if ((float(entry[11]) >= 6.0) & (float(entry[11]) <= 10.0)):
            binaryY = np.append(binaryY, [1])
        else:
            binaryY = np.append(binaryY, [0])

    return binaryY


def binaryCancer(contentsData):
    # (2 for benign append 0; 4 for malignant append 1)
    binaryY = np.array([])

    for entry in contentsData:
        validEntry = True
        for token in entry:

            if (not isnumeric(token)):
                validEntry = False
                break

        if (validEntry):
            if float(entry[10]) == 2.0:
                binaryY = np.append(binaryY, [0])
            if float(entry[10]) == 4.0:
                binaryY = np.append(binaryY, [1])

    return binaryY


binary_wine_quality = t1_2(wine_data)
# print(binary_wine_quality)
binary_cancer_class = binaryCancer(cancer_data)


# print(binary_cancer_class)

# clean data
def t1_3(dataset, columnNum):
    print("original dimensons: ", dataset.shape)

    errorCount = 0
    cleanedLines = np.array([])
    for entry in dataset:

        if len(entry) != columnNum:
            print("there is missing data")
            errorCount += 1
            break

        validEntry = True
        for token in entry:

            if (not isnumeric(token)):
                # print ("there is non-numeric data: ",token)
                errorCount += 1
                validEntry = False
                break

        if (validEntry):
            cleanedLines = np.append(cleanedLines, entry)

    # print("number of error detected: ", errorCount)
    #    print(cleanedLines.shape)

    cleanedLines = np.reshape(cleanedLines, (-1, columnNum))
    # print("dimensons after cleaning: ", cleanedLines.shape)

    # convert to numerical 2d array
    cleanedLines = cleanedLines.astype(np.float)
    # cleanedLines = cleanedLines[:, :-1]

    ## normalize
    # for i in range(len(cleanedLines)):
    #     cleanedLines[i] = normalize(cleanedLines[i])
    # cleanedLines = normalize(cleanedLines)
    cleanedLines = cleanedLines[:, :-1]

    return cleanedLines


wine_data_cleaned = t1_3(wine_data, 12)
# print(wine_data_cleaned)

cancer_data_cleaned = t1_3(cancer_data, 11)
# print(cancer_data_cleaned)


#dataWine = open('winequality-red.csv', 'r')

#dataCancer = open('breast-cancer-wisconsin.data', 'r')

wine_data = t1_1(dataWine, ";", 1)

cancer_data = t1_1(dataCancer, ",", 0)

binary_wine_quality = t1_2(wine_data)

wine_data_cleaned = t1_3(wine_data, 12)
cancer_data_cleaned = t1_3(cancer_data, 11)



# # t1_4
# # plt.axis([0, 1, 0, 1000])
# plt.hist(binary_wine_quality[:], bins='auto')
# plt.title('Wine Quality (0/1) Distribution')
# plt.xlabel('rating')
# plt.ylabel('Sample Count')
#
# # plt.subplot(2,1,1)
# plt.hist(wine_data_cleaned[:, -1], bins='auto')
# plt.title('Wine Quality (1-10) Distribution')
# plt.xlabel('rating')
# plt.ylabel('Sample Count')
# # plt.show()
#
#
# # plt.subplot(2,1,2)
# plt.hist(wine_data_cleaned[:, -2], bins='auto')
# plt.title('Alcohol Percentage Distribution')
# plt.xlabel('Percentage')
# plt.ylabel('Count')
# plt.subplots_adjust(hspace=1)
#
# plt.hist(wine_data_cleaned[:, -4], bins='auto')
# plt.title('pH Distribution')
# plt.xlabel('pH')
# plt.ylabel('Count')
# plt.subplots_adjust(hspace=1)
#
# plt.hist(cancer_data_cleaned[:, -1], bins='auto')
# plt.title('Breast Cancer Class')
# plt.xlabel('Class(2 for benign, 4 for malignant)')
# plt.ylabel('Count')
# plt.subplots_adjust(hspace=1)
#
# plt.hist(cancer_data_cleaned[:, -5], bins='auto')
# plt.title('Bare Nuclei')
# plt.xlabel('Domain id')
# plt.ylabel('Count')
# plt.subplots_adjust(hspace=1)


# data = np.concatenate((x, y.reshape(-1, 1)), axis=1)
# lda=LDA()
# lr = logisticRegression()
# print(kfold(lr, data, 5))

# x = wine_data_cleaned[:, :-1]
# y = binary_wine_quality
#
# x = cancer_data
# y = binary_cancer_class

# from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
# clf = LDA()
# clf.fit(x, y)
# kfold(clf, data, 5)


# from sklearn.linear_model import LogisticRegression
# from sklearn.model_selection import cross_validate
# from sklearn.model_selection import KFold  # import KFold

# kf = KFold(n_splits=5)
# clf = LogisticRegression()
# x,y = process_wines()
# clf.fit(x, y)
# kfold(clf, data, 5)
# print(cross_validate(clf, x,y, cv=3,return_train_score=True))
# for train_index, test_index in kf.split(data):
#     #     print("TRAIN:", train_index, "TEST:", test_index)
#     X_train, X_test = x[train_index], x[test_index]
#     y_train, y_test = y[train_index], y[test_index]
#     clf.fit(X_train, y_train)
#     print(clf.score(X_test, y_test))
