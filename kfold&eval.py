#!/usr/bin/env python
# coding: utf-8

import numpy as np

def kfold(model, dataset, n_splits, shuffle = True):
    if shuffle:
        np.random.shuffle(dataset)
    subsets = np.array_split(dataset, n_splits)
    scores = []
    for index in range(n_splits):
        train = np.concatenate(np.delete(subsets,index))
        test = subsets[index]
        train_x = train[:,:-1]
        train_y = train[:, -1]
        test_x = test[:,:-1]
        test_y = test[:, -1]
        model.fit(train_x, train_y)
        predict_y = model.predict(test_x)
        score = evaluate_acc(test_y, predict_y)
        scores.append(score)
    return sum(scores)/len(scores)

def evaluate_acc(true_y, target_y):
    true_neg = 0
    true_pos = 0
    false_neg = 0
    false_pos = 0
    
    for a,b in zip(true_y, target_y):
        if a == 0:
            if b == 0:
                true_neg += 1
            else:
                false_pos += 1
        else:
            if b == 0:
                false_neg += 1
            else:
                true_pos += 1
    return (true_pos+true_neg)/(len(true_y))

